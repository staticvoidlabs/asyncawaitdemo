﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwaitTesting
{
    public class Program
    {
        static void Main(string[] args)
        {

            StartProcessing();

            Console.Write("done");

            Console.ReadKey();
        }

        private async static void StartProcessing()
        {
            Console.Write("start");

            // TransferInBackground()               start X done X . X..end X X  Y Y  Y Y  Y
            // await TransferInBackground()         start X done X  X  X  X  Y  Y  Y  Y  Y ...end
            // x = await TransferInBackground()     start X done X  X  X  X  Y  Y  Y  Y  Y ...end
            // b = TransferInBackground().Result    start X  X X  X X  Y Y  Y Y  Y done...end
            // TransferInBackground().Wait()        start X  X  X  X  X  Y  Y  Y  Y  Y done...end


            var b = TransferInBackground().Result;

            for (int z=0; z < 3; z++)
            {
                await Task.Delay(500);
                Console.Write(".");
            }
            
            Console.Write("end");
        }


        private static async Task<bool> TransferInBackground()
        {
            int a = await TransferPart1(1);
            int b = await TransferPart2(2);

            return true;
        }

        private static async Task<int> TransferPart1(int x)
        {
            await WasteTime("X", 5);

            return 11;
        }

        private static async Task<int> TransferPart2(int x)
        {
            await WasteTime("Y", 5);

            return 22;
        }

        private static async Task WasteTime(string source, int counter)
        {
            for (int z = 0; z < counter; z++)
            {
                Console.Write(" " + source + " ");
                await Task.Delay(500);
            }

        }


    }
}
